\section{Procurement}
\label{sec:procurement}

\subsection{RFP Considerations and Development}

The acquisition phase is a critical point in any deployment.  Spider II's
Statement of Work (SOW) was assembled with a few key principles in mind. It was
targeted to elicit a diverse set of proposals that could be evaluated within a
budget range. It requested multiple file system and storage technologies to be offered in
a best value, total cost of ownership environment.

% with attractive options that
%could be evaluated in an environment containing a budget range instead of  
%a singularly fixed subcontract target value. 

%The primary challenge in designing and acquiring a parallel file system is
%$how to characterize and define the system architecture. 
The primary challenge in structuring the request for proposal (RFP) for a
parallel file system is how to accurately define the system
characteristics   that a successful PFS must present.  What system(s) will be
served by the file system? What system(s) will be used as storage targets for
file system performance? What is the individual machine-specific I/O workload?
What is the mixed workload? These questions will define the required system
architecture and features, including network technology and topology, and 
lower and upper system performances.  Combining this information with capacity
requirements and budget constraints leads to a target system size and system
architecture.

For the Spider II deployment, the main OLCF compute platform Titan was used as
the reference target for file system performance. Interconnectivity for other
compute platforms was required through Scalable I/O Network
(SION)~\cite{spider_cug12}, the existing OLCF InfiniBand storage area network
(SAN).

Given the HPC storage market during the Spider II procurement, OLCF considered
two RFP response models for Spider II: the ``block storage'' model and the
``appliance'' model. In the block storage model, OLCF would procure block
storage devices, file system servers (i.e. Lustre), and networking equipment
separately and integrate them internally. In the appliance model, OLCF would
procure an integrated solution using vendor-supplied storage, server, and
networking equipment, along with the Lustre software and services in a single
package. OLCF invited vendors to supply proposals implementing one or both
models in order to assess cost, facility and system administrator impact, and
risk.

In crafting the technical requirements, several considerations were identified:

\begin{itemize}

\item{\textbf{Hardware landscape:}} During the period between the Spider I and
Spider II deployments, OLCF conducted significant technology evaluation efforts
in partnership with storage vendors. This equipped OLCF with information
concerning performance targets, desired features, product quality, and vendor
roadmaps. This activity was extremely beneficial to crafting an RFP that was
realistic both technically and financially given the knowledge of vendor
product roadmaps, features, and limitations.

\item{\textbf{Lustre roadmap:}} OLCF is heavily involved in Lustre development
and used Lustre's roadmap projections to determine feature availability in the
project timeline. Requirements surrounding Lustre were incorporated using this
information.

\item{\textbf{Business considerations:}} Total project budget, budget
uncertainty and variability, and market conditions can impact the procurement
schedule and vendor response.  The OLCF designed options in the RFP to increase
budget flexibility. Market conditions can have unintended consequences. For
example, OLCF was forced to delay the release of the Spider II RFP due to the
flooding in Thailand in 2011 that significantly impacted the disk availability
and cost~\cite{thai-disk}. Given that over 20,000 disks were required to meet
the expected capacity and performance targets, such factors must be accounted
for.
%, the expectation for the winner to surpass 20,000 spindles, such factors must
%be accounted for. 

\item{\textbf{Performance:}} OLCF had specific requirements and limitations on
total system performance based on its system architecture, network
architecture, and program-specific requirements. Based on these parameters,
OLCF set specific performance targets in the RFP.  As stated, Titan
%has 600 TB of main memory. Our design target was to checkpoint 75% of Titan's
has 600 TB of main memory. One key design principle was to checkpoint 75\% of
Titan's memory in 6 minutes. This drove the requirement for 1 TB/s as the peak
sequential I/O bandwidth at the file system level. Our earlier tests showed
that a single SATA or near line SAS hard disk drive can achieve 20-25\% of its
peak performance under random I/O workloads (with 1 MB I/O block sizes).  This
drove the requirement for random I/O workloads of 240 GB/s at the file system
level.

\item{\textbf{Scale:}} To accommodate the anticipated budget range, offered
responses must demonstrate scalable units at specific performance and price
points. This structure provides the flexibility to grow the PFS in the future
as needed.
  
%Related to budget uncertainty and system performance, 
%system scale needed to be considered as well. The system scale concerns directly 
%impact several critical evaluation areas such as benchmarking and reliability.   

\end{itemize}

To accommodate issues of performance and scale, the procurement focused on the
\textit{Scalable System Unit} (SSU), a storage building block composed of a
vendor-defined set of storage devices suitable for integration as an
independent storage system. The SOW defined the SSU as the unit of
configuration, pricing, benchmarking, and integration.  Flexibility in SSU
configuration provided for price and performance to be specifically tuned for
the full solution.

Because the RFP allowed for separate procurement of storage hardware, servers,
and the networking equipment, specific performance requirements were engineered
to guarantee that the installed system could meet overall performance goals.
This was necessary because multiple vendors could 
%unlike more tightly-engineered solutions, no single vendor would 
be responsible for overall system performance. 

\lesson{\it {Actively evaluate emerging file system and storage technologies.
Consider vendor hardware and software roadmaps.  Consider acquisition/budget
strategy as applicable to your organization.  Identify technical, financial,
and supply chain risks.  Align RFP release with favorable product development
cycles.}}

\subsection{Benchmark Suite} 
\label{sec:benchmark-suite}

To define the performance targets in the SOW, OLCF developed and released a
benchmark suite based on its experience evaluating storage systems and earlier
workload characterization efforts~\cite{spider1-workload-char}. The benchmark
suite tests several key low-level performance metrics indicative of overall
system performance.  The benchmark tool is synthetic, performing a parameter
space exploration over several variables, including I/O request size, queue
depth, read to write ratio, I/O duration, and I/O mode (i.e. sequential or
random). It includes block-level and file system-level benchmark components.
The block-level performance represents the raw performance of the storage
systems.  The file-system performance also accounts for the software overhead
on top of the block level performance.  By comparing these two benchmark
results, we can measure the file system overhead.  Specific parameters mimic
real I/O workload patterns.  


The block-level benchmark tool, \textit{fair-lio}~\cite{fairlio}, was developed
by OLCF and uses the Linux AIO library ({\it libaio}). It can generate multiple
in-flight I/O requests on disks at specific locations, bypassing the file
system cache. The file system benchmark tool is based on
\textit{obdfilter-survey}~\cite{obdfilter}, a widely-used Lustre benchmark
tool, benchmarking the \textit{obdfilter} layer in the Lustre I/O stack to
measure object read, write, and re-write performance.  Vendors were provided
both the benchmarks and instructions for executing
them~\cite{benchmark-suite-loc}.  Results provided significant insight into the
offered hardware for selection purposes.

\lesson{\it Identify the I/O workloads expected for the PFS.  Include
sequential and random I/O characterization that mimics real workload patterns.
Use benchmarks
%that fully describe the anticipated workload.} edits by SG 
that can comprehensively describe the performance of this mixed workload. }


\subsection{Evaluation}

Offerors were encouraged to propose multiple configuration options to maximize
flexibility in the design of the full system. This significantly increased the
complexity in evaluating the responses, but was widely viewed as worth the
additional effort.

The OLCF evaluated proposal responses based on their technical elements
(including performance, capacity, reliability, power, footprint, maintenance,
and service), delivery schedule, past performance, corporate capability, and
total cost of ownership for the lifetime of the offered solution. During the
evaluation process, each option provided by each vendor was considered.
Ultimately, OLCF chose to purchase a block storage model.
%it was determined that the elements of the storage system would be purchased
%individually and integrated by OLCF (a block storage model was chosen).  
This decision resulted in significant design flexibility and cost savings while
delivering a system that provided adequate delivered performance for users.
However, this decision placed the integration and file system performance risk
on the OLCF.
%the choice also came with its own risks in terms of placing the integration
%risk on the OLCF staff.  
The OLCF accepted this risk since the team possesses substantial experience
deploying, managing, operating, and optimizing large scale file systems.  The
resulting contract for the Spider II file system included 32 PB capacity and
more than 1 TB/s performance in 36 SSUs.
%team was successful in procuring the Spider II storage system with 40 PB
%capacity, 1.3 TB/s of raw block I/O performance (after RAID overhead) in 36
%SSUs.  , 400 kW of power.  , and a footprint of 672 square feet.

\lesson{\it The evaluation criteria must structure the evaluation of all SOW
requirements in a weighted manner such that every element of the vendor
proposal is correctly considered in the context of the entire solution.
Technical elements, performance, schedule, and cost each play an integrated
role in choosing the correct solution. The decision to pursue different technologies 
%block-level storage versus an integrated solution 
must adequately address the inherent risk
of each solution meeting the overall center objectives.}

%Evaluation should be informed by benchmark results as well as a
%careful risk assessment based on local experience deploying and managing
%storage systems.  Extra attention must be paid to consistency and
%reproducibility of the benchmark results provided by vendors.}


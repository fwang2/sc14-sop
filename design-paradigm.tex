\section{Design Principles}

The traditional model of designing a PFS centers around a single computational
resource, which is tightly coupled with the scratch PFS and has exclusive
access to it. Frequently, the scratch PFS is procured through the same contract
as the computational resource.  In designing such a machine-exclusive scratch
file system, one needs to only consider the current and anticipated I/O
requirements of a single system.  Consequently, the design is straightforward.
However, this model does not fit well with the I/O requirements of a collection
of computing resources that must operate together and share data in a
data-centric environment.


To accomplish data sharing in a tightly-coupled machine-exclusive design, one
must 
%{\color{red}A middle of the road An alternative approach would be to create
%data islands of machine specific PFS deployments, and 
link together the various machine specific PFS instances via a data movement
cluster, which would orchestrate data migration jobs upon user requests.  This
scenario is not transparent to the user.  In the absence of a smart data
staging utility, the user is explicitly responsible for data migration
%to accomplish data migration 
before performing analysis or visualization.  Further, the multiple namespaces
can be confusing to the user and hinder productivity.  Additionally, this
scenario can be very costly, as the PFS can easily exceed 10\% of the total
acquisition cost.
%procurement for each compute resource has been measured to be as high as 10\%
%of the system acquisition cost.  
One must also consider the cost of additional infrastructure and interconnect
hardware for transferring the data. 
%This is also costly in scientific productivity and confusing for the user base
%with multiple namespaces and the need to transfer data before performing
%analysis or visualization.  }

In the data-centric approach, data comes first.  The data-centric model focuses
on sharing data among multiple computational resources by eliminating
unnecessary and expensive data movements.  At the same time, it strives to
serve diverse I/O requirements and workloads generated on disparate computing
resources.

In very simple terms, HPC centers are service providers, where 
%In a given center there are 
multiple specialized computational resources provide different services. Some
of these resources are designed to run simulations and generate data. Others
provide post-processing or analysis of the generated data, while some are
responsible for visualization of the raw or reduced data.  The common
denominator in all of these scenarios is data. Data must be shared among these
systems in an efficient manner. Moreover, the users should not face unnecessary
challenges in accessing their data from various systems. Motivated by these
principles, in 2005 the OLCF moved to a data-centric PFS model.  Key design
principles in this effort include \textit{eliminating data islands, reducing
acquisition and deployment costs, and improving data availability and system
reliability}.

%\begin{comment}
\begin{itemize}

\item \textbf{Eliminate data islands.} Users working on one resource, such as a
visualization system, should be able to directly access data generated from a
large-scale simulation on the supercomputer and must not resort to transferring
data. Maintaining the data consistency and integrity across multiple
file systems is a distraction for the users.

\item \textbf{Reduce cost.} In addition to poor usability, machine-exclusive
file systems can easily exceed 10\% of the total acquisition cost.
%take up a substantial percentage of the total system deployment
%cost, often exceeding 10\%.

\item \textbf{Improve data availability and reliability.} A scheduled or an
unscheduled downtime on a supercomputer can render all data on a localized file
system unavailable to the users under the machine-exclusive PFS model.
%scratch file system model.

\end{itemize}
%\end{comment}

\begin{comment}
The data-centric model allows OLCF users to generate data at very-large scales
on Titan and then seamlessly access it from a separate analysis cluster. Such
an integrated data sharing capability significantly increases system usability.
%Based on user feedback, this is our most important capability to maintain for
%the future.
Time and again, user feedback suggests this trait as the most important one to
retain in future systems.
\end{comment}


The data-centric, center-wide file system design provides seamless access to
data from a variety of computational resources within our center. Users need
not concern themselves with data transfers between distinct file systems within
this environment, rather their data is directly accessible from globally
accessible namespaces.  
%Such an integrated data sharing capability significantly increases system
%usability by avoiding expl.  
User feedback describes data-centric capability as critical to their
productivity.

However, there are several challenges in designing a data-centric PFS.  Data
consumers and producers on multiple compute platforms compete for the same
shared resource.  Each compute resource, based on its primary use case,
generates different I/O workloads at different rates.  As an example,
large-scale simulations running on Titan often consume a large percentage of
the available I/O bandwidth and/or IOPs during application checkpoints. These
write-heavy checkpoint/restart workloads can create tens or even hundreds of
thousands of files and generate many terabytes of data in a single checkpoint.
These workloads are bandwidth constrained.  In contrast, the data analytics I/O
workloads, such as visualization and analysis, are latency constrained and
read-heavy.


\begin{comment}
The dominant I/O pattern on large-scale
supercomputers such as Titan is checkpoint/restart, which is a write-heavy
workload. Read I/O cycles in the lifetime of a parallel simulation job running
on a supercomputer, are significantly smaller. On a visualization or an
analysis cluster, on the other hand, read-heavy I/O workloads can be dominant. 
\end{comment}

We note that these different I/O patterns, in isolation, are harmless and can
be serviced more easily by a machine-exclusive scratch file system.  However,
in a data-centric model, they pose a problem as a mixed workload, competing for
the same shared resource. A shared scratch file system experiences these I/O
workloads as a mix, not as independent streams. Our analysis of the I/O
workloads on Spider I PFS \cite{spider1-workload-char} demonstrated a mix of
~60\% write and ~40\% read I/O requests. In some cases, competing workloads can
significantly impact application runtime of simulations or the responsiveness
of interactive analysis workloads. Write and read streams from different
computing systems often interfere because of the difference in data
production/consumption rates.  The I/O streams may not be easily synchronized
between different computing systems because the resources are shared among
multiple users and applications.  One must pay attention to overall mixed
workload, not to individual machine specific I/O patterns, when designing a
data-centric PFS.  Our analysis \cite{spider1-workload-char} showed that a
majority of I/O requests are either small (under 16\,KB) or large (multiples of
1\,MB), where the inter-arrival time and idle time distributions both follow a
long-tail distribution that can be modeled as a Pareto distribution. We
utilized these and other metadata specific characteristics to optimize Spider
metadata servers that are exercised more heavily under varying I/O workload
patterns. 

%We discovered that taking these into
%account resulted into a different final file system design than simply
%optimizing for peak read or write bandwidth. 


\lesson {\it Data-centric center-wide file systems provide significant benefit
to the users in terms of ease of access to their datasets across multiple
platforms, but at the cost of increased contention for a single shared
resource. System architects must weigh the tradeoffs between ease of data
access and the ability to isolate compute platforms from competing I/O
workloads. Weighing these tradeoffs effectively requires a good understanding
of the full breadth of application workloads (both simulation and data
analytics) and  user requirements for data accessibility.}
%, and alternative architectures that are available.}

\lesson {\it While designing a data-centric shared file system, one must
account for detailed I/O workload characteristics.  Peak read/write performance
cannot be used as a simple proxy for designing a scratch file system, because
it may result in either over-provisioning the resources or suboptimal
performance due to a mix of I/O patterns. Good random performance translates
to better operational conditions across a wide variety of application workloads.}

%is a more realistic } While designing a data-centric shared file system, one
%should account for detailed I/O workload characteristics and not simply look
%for highest, advertised, peak read/write performance, but for the random
%performance.} 

\begin{comment}
As a generic rule, depending on
user requirements, design goals and technology limitations, various file system
architectures should be carefully considered. Some of these architectures
include: 

\begin{itemize}

\item Dedicated file systems for major computational
resources that are accessible from data transfer nodes within the facility 

\item  Dedicated file systems for major computational resources that are
cross-mounted across all computational resources 

\item Multiple center-wide file systems that
are specialized for specific work-loads such as simulation checkpoint/restart
or data analytics, these file systems may be accessible from data transfer
nodes or cross mounted across computational resources.

\end{itemize}
\end{comment}


\begin{comment}

Design and procure a PFS based on the mixed and random I/O performance, not 
on the streaming well-aligned performance.}


\lesson {\it Design and procure a PFS based on the mixed and random I/O performance, not 
on the streaming well-aligned performance.}

\paragraph*{\textbf{Lesson Learned}}\textit{Design and procure a PFS
based on the mixed and random I/O performance, not on the streaming well-aligned
performance.}

The utilization of storage space was found to be well maintained below 60-70\%
by periodically triggering purge operations by system administrators.  I/O load
imbalance across servers and RAID controllers was also observed.  This implied
that an individual RAID controller would receive a burst of I/O demands in a
very short time, overloading the controller, while not every RAID controllers
are overloaded at the same time.  
\end{comment}


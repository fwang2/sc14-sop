\section{Higher-level Services}
\label{sec:higher-level-services}

As part of the ongoing operation and maintenance of the center-wide file
systems at OLCF, a number of custom tools and utilities were written to assist
both the users and the system administrators.  These tools provide a mechanism
for exposing the underlying capabilities and performance of the PFS to users.
They also address scalability shortcomings of standard Linux file system
utilities. 

\subsection{Balanced Data Placement}

We have outlined the reasons for designing and operating a data-centric
center-wide file system. While a data-centric file system has its merits, it
has its own challenges. Due to the shared nature of file system, the underlying
system is continuously under contention. Consequently, it becomes more
difficult to expose the underlying raw capability directly to the scientific
applications.  Complex system architecture of the storage system and
non-deterministic end-to-end I/O routing paths exacerbate this problem further,
making it harder to deliver high performance at the client-side.

Therefore, to bridge the gap between the low-level file system capabilities and
applications, we have designed an easy-to-use, portable runtime library
\cite{libPIO}. Our placement library (\texttt{libPIO}) distributes the load on
different storage components based on their utilization and reduces the load
imbalance.  In particular, it takes into account the load on clients, I/O
routers, OSSes, and OSTs and encapsulates these low-level infrastructure
details to provide I/O placement suggestions for user applications via a simple
interface.  Experimental results at-scale on Titan demonstrate that the I/O
performance can be improved by more than 70\% on a per-job basis using
synthetic benchmarks. 

We have integrated libPIO with a large-scale scientific application, S3D, which
is widely run on OLCF resources.  S3D is a large-scale parallel direct
numerical solver (DNS) that performs the direct numerical simulation of
turbulent combustion~\cite{hawkes2005direct}.  S3D is I/O intensive and
periodically outputs the state of the simulation to the scratch file system.
This output is later used for both checkpoint and analysis purposes. The S3D
I/O pattern and performance have been analyzed in ~\cite{yu2008performance}. We
note that only 30 lines of code were needed to be added/modified in the
application for integration with our placement library.  To evaluate the
effectiveness of our approach, we conducted tests in a production (noisy)
environment. We observed substantial gains in S3D I/O performance, up to 24\%
improvement in POSIX file I/O bandwidth. Also, the ease of implementation with
minimal code changes encourages us to believe that libPIO can be widely adopted
by scientific users. 
%Experiments with S3D, a real-world scientific application, showed that our
%balanced placement algorithm is effective in improving write I/O bandwidth in
%a noisy production environment.

%During development, testing, and evaluation of this runtime library, we also
%developed with new methodologies to rigorously test the benefits of a new
%technique in a dynamic environment.

%OLCF is reaching out to more scientific users to integrate the library with
%their scientific applications to promote improved I/O performance with the
%help of our runtime library. Also, as more infrastructure-level details are
%exposed to the applications, it is conceivable (and perhaps preferred) to
%integrate this mechanism with popular middleware I/O libraries such that a
%wide array of applications can take advantage of it without having to
%interface directly.
%
%This path of exploration and integration experience also suggests that, even
%though high performance parallel file systems such as Lustre are designed to
%meet demanding HPC I/O requirements, tighter integration with both the
%upstream simulation machine and downstream backend storage are needed to take
%full advantage of the complex infrastructure underneath. It is probably not
%sufficient to have a flexible configuration system alone, and in this case, a
%certain level of ``programmability'' of the file system is needed.

\lesson{\it The real performance of a data-centric PFS is what users observe.
A file system with a high theoretical peak bandwidth may not deliver that
performance to users under contention.
 %because of the indeterministic routing paths and complex, back-end storage
 %architecture.
It is a good practice to expose low-level infrastructure details to users in an
easy-to-use programmable fashion to achieve higher performance for advanced users.}




\begin{comment}
Due to the shared nature of the file system, the underlying system is
continuously in contention. It is difficult to translate the raw PFS
performance to scientific applications directly.  The complex
architecture of the storage systems and non-deterministic end-to-end I/O
routing paths obscures the performance at the users' end. 

We designed an easy-to-use, portable runtime library written in C, {\sf
libPIO}, that balances the load on different storage components.  In
particular, {\sf libPIO} takes into account the load on clients, I/O routers,
OSSes, and OSTs and encapsulates these low-level infrastructure details to
provide I/O placement suggestions for the user application via a simple
interface.  Experimental results at-scale on the Titan demonstrated that I/O
performance can be improved by more than 50\% on a per-job basis using
synthetic benchmarks and experiments with S3D, a real-world scientific
application, showed that our balanced placement algorithm is effective in
improving write I/O bandwidth in a noisy production environment.

The real performance of a data-centric PFS is what users observe. Spend extra
resources and effort to bridge the gap and expose low-level performance and
capabilities to users.

\end{comment}



%Also, as more infrastructure-level details are exposed to the applications, it
%is conceivable (and perhaps preferred) to integrate this mechanism with popular
%middleware I/O libraries such that a wide array of applications can take
%advantage of it without having to interface directly.

%This path of exploration and integration experience also suggests that, even
%though high performance parallel file systems such as Lustre are designed to
%meet demanding HPC I/O requirements, tighter integration with both the upstream
%simulation machine and downstream back end storage are needed to take full
%advantage of the complex infrastructure underneath. It is probably not
%sufficient to have a flexible configuration system alone, and in this case, a
%certain level of ``programmability'' of the file system is needed.


\subsection{Dynamic Resource Allocator}

As stated in Section~\ref{sec:integration}, scientific applications are
statically distributed over two Spider II namespaces for load balancing and
capacity planning.  However, our static distribution does not fully take into
consideration the dynamic nature of I/O workloads. To address this issue, we
developed a tool called I/O Signature Identifier (IOSI)~\cite{IOSI_fast2014}.
IOSI characterizes per-application I/O behavior from the server-side I/O
throughput logs.  We determined application I/O signatures by observing
multiple runs and identifying the common I/O pattern across those runs. Note
that most scientific applications have a bursty and periodic I/O pattern with a
repetitive behavior across runs. Unlike client side tracing which provides
detailed I/O trace data, our approach provides an estimate of observed I/O
access patterns at no cost to the user and without taxing the storage
subsystem. IOSI can be used to dynamically detect I/O patterns and aid users
and administrators to allocate resources in an efficient manner.

\lesson{\it Smart I/O-aware tools can be built for load balancing, resource
allocation, and scheduling.}

\subsection {Scalable Linux Tools} 

Standard Linux tools do not work well at scale \cite{matney-lci08}.  A good
example is the standard Unix \texttt{du} command. \texttt{du} imposes a heavy
load on the Lustre MDS when run at this scale. Therefore we developed the
LustreDU tool, which gathers disk usage metadata from the Lustre servers once
per day.  This tool is one of the keys to maintaining our policies for file
system usage.  We have seen direct performance degradation when the utilization
of the filesystem is greater than 50\%.

There are other Linux tools inefficient at scale, such as copy (cp), archive
(tar), and query (find). These are single threaded commands, designed to run on
a single file system client. Despite several efforts made to address this issue
in an effective way \cite{matney-cug11, moody-ipdps13, matney-lci08,
kolano2010high}, there is not a complete solution yet, accepted and adopted by
the HPC community.  A collaborative effort between OLCF, Lawrence Livermore
National Laboratory (LLNL), Los Alamos National Laboratory (LANL), and
DataDirect Networks (DDN) has been established to research and develop
efficient, scalable common tools~\cite{fileutils}. As a product of this
collaboration several tools have already been developed, such as parallel copy
(dcp), parallel tar (dtar), and parallel find (dfind).  These tools and future
developments are expected to merge upstream to benefit both our users and the
community at large.

\lesson{\it Standard Linux tools do not work efficiently on large-scale systems. 
%It is desirable to take early initiatives to 
Develop or tune 
some of the basic Linux utilities, such that they can perform well at scale.}
% and do not
%stress a particular storage stack component (e.g., metadata servers).}



%Most standard tools do not scale.  Plan ahead for gathering tools
%from the community or develop your own.}


%
% Removing DDNTool for now - it's only one paragraph and I'm not sure we've got
% room for it.  RGM - 16 April
%
%The final reason for writing tools was to replace manufacturer-supplied
%utilities that were not adequate for the center's needs -- either because the
%center wanted to do something the manufacturer had not considered, or because
%the supplied utilities did not work well at the scales of the Spider and Atlas
%file systems.
% 
%A good example of a utility written because of the first reason was a tool to
%monitor performance information from the disk controllers.  The manufacturer
%did provide tools for monitoring its controllers.  However, those tools were
%GUI-based and designed for interactive use.  If all one needed to do was keep
%an eye on one or two controllers, those tools worked quite well.  However, the
%file system administrators needed to simultaneously monitor performance
%parameters for 72 controllers and the GUI tools did not integrate well with
%their preferred workflow.  Fortunately, the manufacturer published an API for
%accessing performance (and other) data from its controllers, and that allowed
%various custom utilities to be written to access the required data in a form
%that did integrate well.
%

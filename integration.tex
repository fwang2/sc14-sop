\section{Integration}
\label{sec:integration}


A center-wide PFS requires significant integration effort with sustained
reliance on operational infrastructure and expertise.
Figure~\ref{fig:olcf3-spider} shows the Spider II architecture and its
integration into the OLCF infrastructure. In this section we discuss the key
lessons learned from our integration efforts.
%integrating the Spider parallel filesystems into the OLCF's operational and
%compute infrastructure.  


\begin{figure}[!htb]
    \centering
    \begin{tabular}{cc}
        {\includegraphics[width=0.48\textwidth]{./figs/olcf3-spider2-arch.pdf}}\\
    \end{tabular}
    \caption{Integration of Spider PFS and OLCF infrastructure.}
    \label{fig:olcf3-spider}
\end{figure}


\subsection{Infrastructure integration}

Many HPC centers deploy standard services for managing infrastructure;
sufficient attention must be paid to the integration of the storage resources
into the operational infrastructure. In the last decade OLCF has worked to
deeply integrate their storage resources into the operational infrastructure of
the center and has worked with the vendor community to push new features (e.g.
parity de-clustering for faster disk rebuilds and improved reliability
characteristics) into their products that support these functions.

%SV: Sarp this list below does not validate the statement above.  For example,
%as an investment into the Lustre technology, OLCF directly funded the
%development of several file system features including asymmetric router
%notification, high-performance Lustre journaling, and imperative recovery,
%benefiting the Lustre community at large.  OLCF also partnered with multiple
%storage vendors during the Spider II acquisition to ensure that parity
%de-clustering was integrated and available for purchase in their products.  In
%the rest of this section, we will delve into how the storage resources and
%services were integrated into the operational infrastructure.

In 2008, the OLCF initially built out the infrastructure required for the
Spider I file system as a separate instance to provide greater control and
isolation from the rest of the operational infrastructure. The replication of
services and efforts was resource intensive, managed separately, and failed to
leverage the expertise built within the teams that provided services for the
rest of the OLCF. 

Recognizing that the benefits to a separate instance were outweighed by the
additional effort, OLCF revisited its approach in 2010, and integrated the
services that provide centralized authentication and authorization, log
aggregation, configuration management, and other services for the PFS. The new
structure does not relax any control mechanisms or security requirements. 
% to be utilized by the file system cluster.

%
%\begin{itemize}
%
%  \item Configure RSA servers to allow token-based access to Lustre servers.
%
%  \item Capture MAC Addresses for all Lustre servers and put the MAC Address,
%IP Address, and FQDN into the Network Registration System at ORNL so that names
%resolve and DHCP configuration files can be built.
%
%  \item Push all log data from Lustre to center-wide Syslog server; write
%alerts for SEC and Splunk to notify administrators of abnormal conditions.
%
%  \item Build Nagios configurations to monitor the Lustre servers.
%
%  \item Develop and deploy mechanisms for configuration management service to
%manage files both on the Lustre servers and also inside the diskless images. 
%
%\end{itemize}

This approach has yielded simple, robust infrastructure and has allowed the
automation of some routine tasks that previously required system administrator
attention.  Further automation efforts continue.

\lesson{\it The benefits of a single, integrated instance for the
infrastructure of the center-wide systems outweigh the tighter control
mechanisms offered by a disparate or separate file system instance. Where
practical, centralize infrastructure services among disparate systems,
center-wide, to defray expenses, harden those services, reduce administrative
burden, share best practices, reduce inconsistencies, while retaining
coordinated security and control.}  

Below is a description of key features that were integral to the Spider I and
Spider II deployments. 

{\bf Cluster Management and Deployment}

The OLCF has deployed cluster resources (both file system and compute) using
the open-source Generic Diskless Installer (GeDI) \cite{gedi} since 2007. This
mechanism allows the nodes to boot over the control network, \texttt{tftp} an
initial \texttt{initrd}, and then 
%use read-only nfs mount the root file system.  
mount the root file system in a read-only fashion.  Using diskless servers for
the Lustre Object Storage Servers (OSS) and Metadata Servers (MDS) reduces the
overall cost of the storage system because these nodes do not require RAID
controllers, disk backplanes, cabling, disk carriers, or the physical hard
drives.  This materially reduces the acquisition and maintenance costs. 

Custom scripts have been developed to create host-specific configuration files
for boot services and the Lustre file system for the boot process so that the
services can be started appropriately by the Linux \texttt{init} process. OLCF
added a new feature to GeDI to support Spider II deployment where configuration
files are built as the node boots, but before the service that needs the
configuration file is started by the OS.  This feature mimics the Linux model
of \texttt{rc.local.d} for scripts that need to be part of the runtime.
Scripts in \texttt{/etc/gedi.d} are run in integer order  to build
configuration files for network configuration, the InfiniBand srp\_daemon
configuration, and the InfiniBand Subnet Manager.  These scripts also create
RAM disks for the node-specific files in \texttt{/etc}, \texttt{/var}, and
\texttt{/opt}.

This robust and repeatable image build process allows for rapid changes to both
the operating system and the Lustre software base. The process is integrated
with the center's change and configuration management system, BCFG2
\cite{bcfg2}, so that the effects of specific changes are easily determined.
The change management process is consistent with the site cyber security
policies. OLCF modifications to BCFG2 support diskless clients allowing for
fast convergence to a node's configuration.  This structure can positively
impact mean time to repair (MTTR).
%OLCF identified and implemented a solution for BCFG that addresses diskless
%clients, allowing for fast convergence on a node's configuration to reduce the
%file system down times.
 
%One key design requirement for Spider deployments was seamless integration
% with the operational OLCF infrastructure and complying with the site cyber
% security policies. OLCF exclusively uses the configuration management engine
% BCFG2 \cite{bcfg2}. 
%OLCF identified and implemented a solution for BCFG that
%addresses diskless clients, allowing for fast convergence on a node's
%configuration. 
%This can reduce the file system down times.

\lesson{\it Build PFS clusters using diskless nodes to increase reliability and
reduce complexity and cost.  Build repeatable, reliable processes that rely on
configuration and change management.}


%\subsubsection{Configuration Management}

%{\bf Configuration Management}

%Using configuration management is an accepted, required system administration
%practice. Having version control on everything managed via configuration
%management allows for a full history of what change was made and why. 


%\lesson{\it Using configuration management is required; using a single
%configuration management system center-wide is the best solution.}


%\lesson{\it Using configuration management is required; at OLCF, 
%center-wide, single configuration management system seems to be the most
%desirable solution for our needs and has worked well.}


%\subsubsection{Monitoring}

{\bf Monitoring}

A center-wide file system resource must be continuously available to its
clients. OLCF has developed mechanisms for providing better reporting about the
health of the file system \cite{Spider_CUG2010} through the OLCF's monitoring
framework provided by Nagios \cite{nagios}.  Vendor tools frequently exhibit
limited capabilities, high overheads, and inability to scale. To address these
weaknesses, we developed light-weight bandwidth monitoring and performance
quality assurance tools. Where possible, these tools are open-sourced to the
community to support their efforts.

During the Spider I operational period, OLCF developed a utility called Lustre
Health Checker that provided visibility into internal Lustre health events,
giving system administrators a coherent collection of associated errors from a
Lustre failure condition. Additional utilities were extended to coalesce
physical hardware events on the Lustre servers to help identify and debug
failure conditions. These two features allowed system administrators to
discriminate between hardware events and Lustre software issues
\cite{Hill_BP_Widow}. 


%Initial steps involved providing the status of the Lustre health check file so
%that system administrators could investigate when Lustre triggered an internal
%health event. Providing the status of the physical servers was another effort
%that delivered significant benefits as many times problems with the file system
%were a direct result of a problem with the physical hardware. The status of the
%physical host was an important tool in providing a stable file system resource
%\cite{Hill_BP_Widow}.

To monitor the InfiniBand adapter and network, custom checks were written
around the standard OFED \cite{OFED}  tools for HCA errors and network errors.
These alerts provide detection of problems between the server and the InfiniBand
connected storage or problems in the network between the Lustre server and
clients. Single cable failures can cause performance degradation in accessing
the file system. OLCF has developed procedures for diagnosing a cable in-place
and provided these procedures to the manufacturer.
%These cables are replaced nominally during a system outage, but in some cases
%are replaced in production with a performance degradation to the PFS resource. 

DDN Tool~\cite{ddntool10:ross} was developed to monitor the DDN S2A and SFA
storage system RAID controllers.  This tool 
%monitors the connections with the RAID storage controllers, 
polls each controller for various pieces of information (e.g. I/O request
sizes, write and read bandwidths) at regular rates and stores this information
in a MySQL database. Standardized queries and reports support the efforts of
the system administrators.

%Collecting this
%information has no performance impact on the compute nodes, does not require
%any user effort, and has minimal overhead on the storage servers. Further, the
%data is collected over the storage servers’ Ethernet management network,
%without interfering with the applications I/O. 

\lesson{\it A robust monitoring/alerting platform coupled with analysis tools
reduces cluster and file system administration complexity for large parallel
file systems. Leverage open-source and vendor supported tools and utilities to
overcome inherent weaknesses of basic tools.}

\subsection{Compute-side integration}

The Lustre client and server software stacks must remain compatible
%through multiple feature releases 
with each other over the life of the file and storage system. Titan is a unique
resource that supports testing at extreme scale. 
%To support the need for large scale testing, the hardware subsystem. 
To benefit the OLCF and the community at large, the OLCF allocates the Titan
and the Spider PFS for full scale tests of candidate Lustre releases.
% resources for performing full scale tests of a candidate Lustre releases as
% test resources at this scale do not exist outside of OLCF. 
These tests identify edge cases and problems that would not manifest themselves
otherwise.
% Recent experiences have shown that even through diligent testing problems can
% be missed and must be addressed by rolling back the change and revisiting the
% release for extended testing.

\lesson{\it Develop a center roadmap for features that are important; align it
with vendor and developer roadmaps and rigorously test against production
workloads. Leverage the benefit of external test resources that can reveal
problems at scale.}


\subsection{Capacity Planning, Provisioning, and Management}

The OLCF examined two strategies for deploying a parallel file system: a
single large capacity namespace and multiple smaller namespaces. 

A single namespace has several benefits. It is easier for users, provides the
most bandwidth to applications, and provides the most flexibility in managing
the capacity allotted to each project. While providing a single namespace to
users is a straightforward  way to deploy a data-centric resource, it
introduces key disadvantages that can make it impractical to large-scale
centers, such as the OLCF. Lustre supports a single metadata server per
namespace. This limitation cannot sustain the necessary rate of concurrent file
system metadata operations for the OLCF user workloads. With a single
namespace, any problem has the potential to propagate to all other OLCF
resources. These technical challenges are significant enough to divide the
capacity space into smaller file system namespaces. The authors acknowledge
that the Lustre 2.4 version introduced the Distributed Namespace (DNE) feature.
Currently, some legacy Lustre clients block implementation of this feature at OLCF.
%current limitations placed by the client Lustre version on certain compute
%platforms in OLCF are blocking implementation of this feature.  still feel
%that multiple namespaces have more benefit than a single namespace.  With a
%single namespace, any problem has the potential to propagate to all other OLCF
%resources. These technical challenges are significant enough to divide the
%capacity space into smaller file system namespaces.  
We recommend using both DNE and multiple namespaces, concurrently.

Splitting the namespace across the storage hardware will reduce the delivered
bandwidth to any one specific application. However, our experience has shown
that other factors will limit the users' observed performance before the
reduction in hardware will. Users are not confined to a single namespace at the
OLCF.  They may request access to all namespaces and use them as they see fit.
%simply must request the additional resource and perform the necessary changes
%to their I/O routines to utilize multiple directories across two separate
%filesystems.
This strategy can provide improved performance for their application workloads.

Next, OLCF developed a model that classifies projects based on their capacity
and bandwidth requirements. The projects were then distributed among the
namespaces.  This model allowed the OLCF to manage the capacity and bandwidth
more evenly across the namespaces for Spider I (four namespaces) and Spider II
(two namespaces) file systems. 

The Spider file systems are scratch. To maintain these volumes, the OLCF
employs an automatic purging mechanism. Files that are not created, modified,
or accessed within a contiguous 14 day range are deleted by an automated
process. This mechanism allows for automatic capacity trimming. The OLCF as
well as many other HPC centers that use Lustre note a severe performance
degradation after the resource is 70\% or more full.  

\lesson{\it Create multiple namespaces and use the operational expertise to
segregate intense workloads and manage overall capacity; use an automatic
purging mechanism to control capacity. Ensure that the acquisition strategy
provides sufficient total storage such that performance is maintained up to
typical performance degradation points. This may require capacity targets 30\%
or more above aggregate user workload estimates.}

%Acquisition must provide enough usable
%storage to keep the nominal PFS utilization level under the performance
%degradation point.}

\subsection{Product Extensions to Satisfy Operational Needs}

Commercial product offerings may not fully meet the requirements of the HPC
centers. OLCF identified functionality, reliability, and performance gaps in
Lustre. To close these gaps, OLCF direct-funded developments efforts through
multiple providers to produce features including asymmetric router
notification, high-performance Lustre journaling, and imperative recovery, all
benefiting the Lustre community at large. OLCF continues to contribute to
Lustre development efforts through direct funding and community consortium
participation (i.e., OpenSFS).

\subsection{Human Errors}

%System administrators are not the superhuman individuals that they are viewed as;
% mistakes happen.

%Even when all of the lessons stated above are followed, to err is human and
%failures happen.  
Despite precautions and prescriptive policy, human error 
%(sometimes even from experienced system administrators) 
may affect the operation of HPC centers in a significant and undesirable
manner. 
%Fortunately, our experience in this regard has been limited. 
An incident in 2010 demonstrates the need to anticipate mistakes, planning for
their occurrence, but limiting their effect through careful design, effective
policy.  
%led us to put in place several proactive measures. 
%taught us enough of lesson to take even more proactive measures.

A disk was replaced in a storage enclosure.  In the Spider I file system
design, 10 disks in a RAID 6 set were evenly distributed across five disk
enclosures.  During the disk rebuild, the connection between the storage
controller to the disk enclosure was interrupted due to a hardware failure and
failed over to the other storage controller as designed. 
%was to the disk enclosure was dropped.
This unit was returned to production while still in a rebuild state, which
meets the design specification.  However, 
%The impact of the error condition  was not well understood and the file system
%was returned to the production. 
eighteen hours later, the affected storage array was taken offline, while still
in the rebuild mode, losing journal data for more than a million files managed
by that controller pair.
% and the file system was back in production in degraded mode.  
%This action affected over 1 million files resided on the storage that was
%affected.  
Recovery of the lost files took more than two weeks, with 95\%  successful
recovery rate.
%OLCF undertook a 24x7 effort for over 2 weeks to mitigate the issue and
%recovered 95\% of the files on the affected storage. 
This was a drawback in the design of Spider I; architected design used 5 disk
enclosures per storage controller pair instead of 10.  A design using 10
enclosures per storage controller pair would have tolerated this failure
scenario. 

% a decision was made o use 5 disk enclosures instead of 10 per storage
% controller pair.  This decision was made for space, power, and cost reasons
% and if we had made the alternate decision, we would not have faced this
% issue. 

\lesson{\it Failures due to human error will occur.  To the greatest extent
possible, the design, process, procedures, and policy should be structured to
minimize the impact of these errors. Recognize that individual failures of
hardware or software can be compounded by human error to produce unanticipated
error conditions with significant impact.} 

%Having the monitoring,
%procedures, and experience to recover from some of them are one of the bigger
%challenges.}

\section{End-to-end Performance Tuning}
\label{sec:performance-tuning}

Large-scale supercomputing storage infrastructure is a complex distributed
system that needs to be tuned at several levels to achieve good end-to-end I/O
performance.  Several factors should be considered while tuning the performance
including the storage hardware subsystem, the file system, user workloads, the
clients, and the routing infrastructure.  Vendor advertised peak sequential I/O
performance numbers will realistically not be achievable on systems under
real-life workloads.

%Sequential large block performance compared to random small
%block performance will be quite different.  


While there will be some differences between different deployments, the
multiple layers of any large-scale storage system will generally comprise block
devices, the interconnect between the block devices and the file system
servers, file system servers and the file system itself, and the interconnect
between the file system servers and file system clients. Some configurations
will include I/O routers between the file system servers and the clients
(Figure~\ref{fig:olcf3-spider}).  Each layer must be separately benchmarked to
ensure proper operation and optimal performance. Our testing methodology is
bottom up. We start our evaluation from the block-level to establish a baseline
performance and work our way up the stack.  We establish an expected
theoretical performance of a given layer and then compare that with the
observed results. Each additional layer introduces new bottlenecks and
re-defines the expected performance. 

Our deployment-specific subsystems include the 36 block storage system units
(SSUs), the Lustre file system, the scalable I/O network (SION), the Lustre
router nodes, and the Lustre clients on the Titan compute nodes themselves.
Other OLCF resources are connected to Spider over the same SION and each
resource has its own set of Lustre routers. Each one of these layers is a large
system with its own idiosyncrasies.  For example, Spider's block storage
subsystem comprises 20,160 2 TB near-line SAS disks that are organized into
2,016 object storage targets (OSTs). 288 storage nodes are configured as Lustre
object storage servers (OSS). 440 Lustre I/O router nodes are integrated into
the Titan interconnect fabric. Titan provides 18,688 clients, all performing
I/O.  Each of these layers must be tuned and optimized to derive optimal
performance from the end-to-end I/O stack.  The Spider storage system delivers
a peak throughput of 1 TB/s by optimizing each of these subsystems and how they
interact with each other.  

%The same tools mentioned in Section
%\ref{sec:procurement} are used to benchmark the lowest layers.
%\textit{fair-lio} benchmarks ensure that the block devices are performing at
%optimal rates.  Next, \textit{obdfilter-survey} is run to ensure that the
%Lustre layer does not add significant overhead. The \textit{lnet-selftest} tool
%is then used to benchmark the network.  Finally, benchmarks such as
%\textit{IOR} are used for full end-to-end tests.

\lesson{\it Build the performance profile for each layer in the PFS, from the
bottom up. Quantify and minimize the lost performance 
%This helped in performance debugging, specifically in quantifying the lost
%performance 
in traversing from one layer to the next along the I/O path.  Systematically
calibrate the performance baselines for each layer correctly and fix the
bottlenecks at each point.}

%\lesson{\it Build a performance profile for every layer in the PFS, bottom to
%top. Work your way up layer by layer, re-baselining the PFS performance.} 

\subsection{Tuning the Block Storage Layer}


The underlying block storage layer must be well-tuned and working as expected
within a tight performance envelope to obtain consistent parallel I/O
performance. Spider II disks are organized as RAID level 6 arrays (8 data and 2
parity disks). Each RAID group is then used as a Lustre Object Storage Target
(OST). When a file is stripped over multiple OSTs to achieve higher
performance, all underlying OSTs must perform as expected. During deployment,
it was discovered that identifying and eliminating slow disks, even if they
were functional with no errors, helps to significantly reduce the variance and
tighten the performance envelope across the RAID groups.  Block-level
benchmarks were run to ensure that 
%baseline RAID group performance on a single SSU 
the slowest RAID group performance over a single SSU was within the 5\% of the
fastest and across the 2,016 RAID groups the performance varied no more than
the 5\% of the average.  We conducted multiple rounds of these tests,
eliminating the slowest performing disks at each round.  We derived the 5\%
threshold empirically based on our experience with the Spider I deployment and
operations.  For all tests, the RAID groups were organized into performance
bins and disk level statistics were gathered from the lowest performing set of
groups. Disks accumulating higher I/O request service latencies were identified
and replaced.  Overall, during the deployment process we replaced around 1,500
of 20,160 fully functioning, but slower, disks.  After deployment, the same
process was repeated at the file system level and we eliminated approximately
another 500 disks. This is a continuous effort for the lifetime of the PFS, but
the initial effort helped in improving the per OST and aggregate I/O
performance immensely.  In production, the initial requirement for 5\%
variability among RAID groups was determined to be prohibitive and was
contractually adjusted to 7.5\%.

\lesson{\it Variance caused by slow disks in a data-centric PFS will negatively
impact  overall performance.  It is critical to identify and replace them. It
is essential to repeat this process periodically for the lifetime of the PFS to
ensure consistent sustainable performance. Seek disk performance gains through
manufacturer software and firmware updates.}

 
\subsection{Tuning the I/O Routing Layer}
%: Fine-Grained Routing}

Large-scale distributed systems must take advantage of data locality to avoid
the performance penalty of excessive data movement.  Titan's network is based
on the Cray Gemini interconnect that is configured as a 3D torus.  I/O routers
must be used to bridge Titan's Gemini interconnect to Spider's InfiniBand
fabric.  Spider II was designed with a decentralized InfiniBand fabric that
consists of 36 leaf switches and multiple core switches.  The I/O from Titan's
compute clients are funneled through the I/O routers to the Spider storage
system.  

Considerable effort was directed towards calculating the router placement on
Titan's 3D torus.  This required significant understanding of the network
topology. Several factors were taken into consideration such as network
congestion (both on the Gemini and I/O InfiniBand networks), physical and
logical dimensions of the Titan layout, number of clients and routers, and
Gemini interconnect injection rates and link speeds \cite{rtrplacement}.
Additional information about Gemini's performance characteristics and routing
algorithm relevant to the placement decisions are available elsewhere
\cite{ezell-gemini}.  The final I/O router placement in Titan's 3D torus
network topology is shown in Figure~\ref{fig:router-3d}.  Each box in the
figure represents a cabinet, where $X$ and $Y$ denotes the dimensions in
Titan's 3D torus topology.  Colored boxes represent cabinets containing at
least one I/O module.  Similar colors correspond to identical ``router
groups.''  Router groups roughly correspond to SSU indices, with each group
being assigned to four InfiniBand switches that provide connectivity between
routers and the Lustre file servers.  Each I/O module contains 4 I/O routers,
with each router on the module connecting to a different InfiniBand switch.

\begin{figure}[!htb]
    \centering
    \begin{tabular}{cc}
        {\includegraphics[width=0.45\textwidth]{./figs/FGR_Placement}}\\
    \end{tabular}
    \caption{Topological $XY$ representation of Titan's Lustre routers.}
    \label{fig:router-3d}
\end{figure}

For our mixed workloads, it was vital to physically distribute the routers in a
way that ensures fair sharing for all of the compute clients.  The average
distance between any client and its closest I/O router should be optimized.
OLCF devised a fine-grained routing (FGR) technique~\cite{dillow-ipccc} to
optimize the path that I/O must traverse to minimize congestion and latency.
At the most basic level, FGR uses multiple Lustre LNET Network Interfaces (NIs)
to expose physical or topological locality.  Each router has an InfiniBand-side
NI that corresponds to the leaf switch it is plugged into.  Clients choose to
use a topologically close router that uses the NI of the desired destination.
Clients have a Gemini-side NI that corresponds to a topological ``zone'' in the
torus.  The Lustre servers will choose a router connected to the same
InfiniBand leaf switch that is in the destination topological zone.

\lesson{\it Network congestion will lead to suboptimal I/O performance.
Identifying hot spots and eliminating them is key to realizing better
performance. Careful placements of I/O processes and routers and better routing
algorithms, such as FGR, are necessary for mitigating congestion.}


\subsection{Scaling Tests}

%The OLCF tuned the I/O performance end-to-end, across the entire software
%stack.  
Part of end-to-end performance tuning is scaling studies.  While hero runs
provide a good indicator of the peak throughput, scaling tests provide a more
realistic picture of the I/O performance. 

We used IOR, a common synthetic I/O benchmark tool~\cite{iorxt}.  Our
custom benchmark suite discussed in Section~\ref{sec:benchmark-suite} was
primarily designed for small-scale block-level performance testing of a storage
system, suitable for vendor responses to an acquisition activity. IOR, on the
other hand, provides a readily available mechanism for testing the file
system-level performance at-scale. 

The parameter space for the scaling tests for a file system-level performance
study is huge. Tests require a systematic approach of conducting controlled
experiments. As an example, Figures~\ref{fig:transfer-size-scaling}
and~\ref{fig:client-scaling} show a sample scaling study conducted on a single
Spider II file system namespace. Since each Spider II namespace
spans half of the available storage hardware resources, the expected
the top-level performance is also halved. 
%We used the Spider II ``thin'' file system to preserve the integrity of the
%production file system during testing.  

As an experiment, we first sought the optimal transfer size per I/O process. To
do this, we fixed the client size, the total amount of data per I/O process and
the test duration and varied the I/O transfer size per I/O process.
We used IOR in the file-per-process mode.
Figure~\ref{fig:transfer-size-scaling} shows the results.  We identified that
the best performance for writes can be obtained by using a 1 MB transfer size.
We then fixed the transfer size to 1 MB and scaled the number of clients (I/O
writer processes).  Figure~\ref{fig:client-scaling} presents the results. While
these two tests are not indicative of the best possible performance that a
single Spider II namespace can provide (as further optimizations are underway),
they showcase how large-scale file systems can be systematically probed for
performance. As can be seen from Figure~\ref{fig:client-scaling}, given its
current configuration, a single namespace can scale almost linearly up to 6,000
clients and then provide relatively steady performance with respect to
increasing number of clients. For both tests, we used a quiet system (Titan and
Spider II were idle) and we let the scheduler place test clients on Titan
compute nodes (i.e. random placement). This placement is optimized for
nearest-neighbor communication, not for I/O. Each test ran for multiple
iterations and each iteration ran for 30 seconds (stone wall option) to
eliminate stragglers.

It is also worth mentioning that, as of this writing, the Spider II storage
controllers were recently upgraded with faster CPU and memory.  This upgrade
improved the overall performance characteristics of the Spider II system. As a
result of this upgrade, we observed 510 GB/s of aggregate sequential write
performance out of a single Spider II file system namespace, versus 320 GB/s
before the upgrade. IOR was used for this test in the file-per-process mode
with 1 MB I/O transfer sizes. The peak performance was obtained using only
1,008 clients against 1,008 OSTs. The clients were optimally placed on Titan's
3D torus such that it minimized network contention for I/O. 
%This test was conducted on a quiet system.  
Further testing is scheduled to obtain scaling performance numbers from each
namespace. 

%{\color{red} mark}

%ORNL and OpenSFS have funded several projects to modernize and update the
%Lustre code base to improve performance.  SMP scaling, \textit{pdirops},
%Imperative Recovery, and \textit{ptlrpcd threadpool} are just a few examples.

\begin{comment}
There are other factors to be examined with respect to the I/O performance,
such as CPU utilization. A very useful tool to aid in understanding CPU usage
is the \textit{perf} performance analysis tool for Linux.  It uses the
kernel-based performance counter subsystem to sample and analyze system
behavior.  The \textit{perf top} tool is invaluable in finding bottlenecks.
This tool has led to several settings changes and even code changes to reduce
overall CPU usage.
\end{comment}


\begin{figure}[!htb]
    \centering
    \begin{tabular}{cc}
        %{\includegraphics[width=0.46\textwidth]{./figs/4k-stonewall-wrt-transfersize-max-write.pdf}}\\
        {\includegraphics[width=0.46\textwidth]{./figs/4k.pdf}}\\
    \end{tabular}
    \caption{Performance analysis as a function of I/O transfer size on a single Spider II namespace.}
    \label{fig:transfer-size-scaling}
\end{figure}

\begin{figure}[!htb]
    \centering
    \begin{tabular}{cc}
        %{\includegraphics[width=0.46\textwidth]{./figs/fpp-1mb-transfer-size-client-scaling-max-write-bw.pdf}}\\
        {\includegraphics[width=0.46\textwidth]{./figs/fpp.pdf}}\\
    \end{tabular}
    \caption{Performance analysis as a function of number of clients on a single Spider II namespace.}
    \label{fig:client-scaling}
\end{figure}

\begin{comment} 
\paragraph*{\textbf{Lesson Learned}}\textit{Conduct controlled
experiments for analyzing the I/O performance of a large-scale, complex system.
Repeat experiments multiple times. Do not modify more than one variable at a
time during experiments. Document and catalog your test configurations, tools,
and results. Prepare a post-mortem test analysis report as soon as possible
after your tests.} 
\end{comment}

%Deploying and operating large-scale systems is expensive.  Every hour they are
%not running/servicing production workloads impacts scientific productivity.
%incurs monetarily costs.  

\lesson{\it Running scaling I/O performance tests for large number of variables at
extreme-levels can consume significant operational time, but yields very
important scaling data. It is critical to carefully reduce the parameter space
to shorten the test window and minimize the cost and program impact. In the
end, the data obtained should be used to further configure and optimize the
end-to-end I/O stack. Same scaling data should be used to guide the
expectations for operational performance.} 

\subsection{Sustainable Lifetime Performance}

Sustaining storage system performance through the lifetime of the system
requires a \textit{performance quality assurance (QA)} approach.  Just as one
would perform periodic QA of a data repository to ensure that the contents are
valid, one needs to conduct performance QA to ensure that the storage system
can continually deliver the desired performance metrics.  During the initial
deployment, the storage system is fresh, without any user data.  This allows
destructive tests that build and tear down file systems, while tuning and
measuring performance.  Performance testing will require writing and reading
large volumes of data, which is not feasible on a live storage system.  To this
end, the Spider file systems were provisioned with a small part of each RAID
volume reserved for long-term testing.  While it only represents a small
percentage of the total hardware capacity, it can be used to stress the entire
system.  This \textit{``thin''} file system, which contains no
user data, can be used to run destructive benchmarks even after Spider has been
put into production.  It also allows for performance comparisons between full
file systems and those that are freshly formatted.

\lesson{\it Plan and design for test resources for the lifetime of the PFS.
Mechanisms such as a thin file system can accommodate the destructive nature of
some of these tests.  The capacity for these thin file systems are minimal, but
should be accounted for in the acquisition process.  Additional resources for
testing and validating hardware and software upgrades before being deployed on
production systems, is extremely beneficial over the life time of the PFS. A
small-scale replica of the PFS building blocks can suffice for this testing and
validation purposes.}

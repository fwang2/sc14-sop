all:
	latexmk -pdf paper

clean:
	latexmk -C paper
	rm -f *.fdb_latexmk
	rm -f *.log
	rm -f *.aux
	rm -f *.dvi
	rm -f *.bbl
